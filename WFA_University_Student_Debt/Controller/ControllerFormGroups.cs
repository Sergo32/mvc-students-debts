﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model;
using WFA_University_Student_Debt.Model.Entities;
using System.Windows.Forms;

namespace WFA_University_Student_Debt.Controller
{
    class ControllerFormGroups
    {
        private FormGroups form;
        private DbManager dbManager;
        public ControllerFormGroups(FormGroups form)
        {
            this.form = form;
            dbManager = DbManager.GetInstance();
        }

        public void FillListBoxGroups()
        {
            form.listBoxGroups.DataSource = null;
            form.listBoxGroups.DataSource = dbManager.TableGroups.GetAllGroups();

        }

        public void ClearFields()
        {
            form.textBoxId.Clear();
            form.textBoxName.Clear();
        }

        public void FillFieldsFromSelectedGroups()
        {
            if (form.listBoxGroups.SelectedItem == null)
            {
                return;
            }

            EntitieGroups groups = (EntitieGroups)form.listBoxGroups.SelectedItem;

            form.textBoxName.Text = groups.Name;
            form.textBoxId.Text = groups.Id.ToString();
        }

        public void AddGroup()
        {
            if (form.textBoxName.Text == string.Empty)
            {
                MessageBox.Show("Заполните Имя");
                return;
            }

            try
            {
                EntitieGroups groups = new EntitieGroups()
                {
                    Id = int.Parse(form.textBoxId.Text),
                    Name = form.textBoxName.Text
                };

                dbManager.TableGroups.AddGroups(groups);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте Имя группы.Она не может быть как у других групп");
                return;

            }
        }


        public void DeleteGroup()
        {
            if (form.textBoxId.Text == string.Empty)
            {
                MessageBox.Show("Задолжность выберите для удаления");
                return;
            }
            int id = int.Parse(form.textBoxId.Text);

            dbManager.TableGroups.DeleteByIdGroup(id);

        }

        public void UpdateGroup()
        {
            if (form.textBoxName.Text == string.Empty)
            {
                MessageBox.Show("Заполните Имя");
                return;
            }

            try
            {
                EntitieGroups groups = new EntitieGroups()
                {
                    Id = int.Parse(form.textBoxId.Text),
                    Name = form.textBoxName.Text
                };

                dbManager.TableGroups.UpdateByIdDebts(groups);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте Имя группы.Она не может быть как у других групп");
                return;

            }
        }
    }
}
