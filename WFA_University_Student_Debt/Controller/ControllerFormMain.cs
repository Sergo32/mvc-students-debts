﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model;
using WFA_University_Student_Debt.Model.Entities;
using System.Windows.Forms;



namespace WFA_University_Student_Debt.Controller
{
    class ControllerFormMain
    {

        private FormMain form;
        private DbManager dbManager;

        public ControllerFormMain(FormMain form)
        {
            this.form = form;
            dbManager = DbManager.GetInstance();
        }


        public void FillDataGridStudents()
        {
            form.dataGridViewStudents.DataSource = null;
            form.dataGridViewStudents.DataSource = dbManager.TableStudents.GetAllStudents(dbManager.TableDebts.GetAllDebts(), dbManager.TableGroups.GetAllGroups());
            form.dataGridViewStudents.Columns["Id_debts"].Visible = false;
            form.dataGridViewStudents.Columns["Id_Group"].Visible = false;
        }

        public void FillComboBoxDebts()
        {
            form.comboBoxDebts.DataSource = null;
            form.comboBoxDebts.DataSource = dbManager.TableDebts.GetAllDebts();
        }

        public void FillComboBoxGroups()
        {
            form.comboBoxGroups.DataSource = null;
            form.comboBoxGroups.DataSource = dbManager.TableGroups.GetAllGroups();
        }



        public void ClearFields()
        {
             form.comboBoxGroups.SelectedItem = -1;
            form.comboBoxDebts.SelectedItem = -1;            
            form.maskedGradebook.Clear();
            form.textBoxName.Clear();
            form.textBoxMiddleName.Clear();
            form.textBoxSurname.Clear();
            form.textBoxId.Clear();

        }

        public void FillFromSelectedStudents()
        {
            if (form.dataGridViewStudents.SelectedRows.Count == 0)
            {
                return;
            }

            EntitieStudents students = (EntitieStudents)form.dataGridViewStudents.SelectedRows[0].DataBoundItem;

            form.textBoxSurname.Text = students.Surname;
            form.textBoxMiddleName.Text = students.Middle_name;
            form.maskedGradebook.Text = students.Gradebook.ToString();
            form.textBoxName.Text = students.Name;
            form.textBoxId.Text = students.Id.ToString();
            form.comboBoxDebts.Text = students.Debts.ToString();
            form.comboBoxGroups.Text = students.Groups.ToString();

        }

        public void AddStudent()
        {

            if (form.textBoxName.Text == string.Empty)
            {
                MessageBox.Show("Заполните имя");
                return;
            }
            if (form.textBoxMiddleName.Text == string.Empty)
            {
                MessageBox.Show("Заполните отчество");
                return;
            }
            if (form.textBoxSurname.Text == string.Empty)
            {
                MessageBox.Show("Заполните фамилию");
                return;
            }
            if (form.comboBoxDebts.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите предмет");
                return;
            }
            EntitieDebts debts = (EntitieDebts)form.comboBoxDebts.SelectedItem;
            EntitieGroups groups = (EntitieGroups)form.comboBoxGroups.SelectedItem;

            try
            {
                EntitieStudents students = new EntitieStudents()
                {
                    Surname = form.textBoxSurname.Text,
                    Name = form.textBoxName.Text,
                    Middle_name = form.textBoxMiddleName.Text,
                    Gradebook = int.Parse(form.maskedGradebook.Text),
                    Id_debts = debts.Id,
                    Debts = debts,
                    Id_Group = groups.Id,
                    Groups = groups
                };
                dbManager.TableStudents.AddStudent(students);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте номер зачетки.Она не может быть как у других студентов");
                return;

            }

        }


        public void DeleteStudent()
        {
            if (form.textBoxId.Text == string.Empty)
            {
                MessageBox.Show("Cтудента выберите для удаления");
                return;
            }
            int id = int.Parse(form.textBoxId.Text);

            dbManager.TableStudents.DeleteByIdStudent(id);

        }

        public void UpdateStudent()
        {
            if (form.textBoxName.Text == string.Empty)
            {
                MessageBox.Show("Заполните имя");
                return;
            }
            if (form.textBoxMiddleName.Text == string.Empty)
            {
                MessageBox.Show("Заполните отчество");
                return;
            }
            if (form.textBoxSurname.Text == string.Empty)
            {
                MessageBox.Show("Заполните фамилию");
                return;
            }
            if (form.comboBoxDebts.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите предмет");
                return;
            }

            EntitieDebts debts = (EntitieDebts)form.comboBoxDebts.SelectedItem;
            EntitieGroups groups = (EntitieGroups)form.comboBoxGroups.SelectedItem;

            try
            {
                EntitieStudents students = new EntitieStudents()
                {
                    Id = int.Parse(form.textBoxId.Text),
                    Surname = form.textBoxSurname.Text,
                    Name = form.textBoxName.Text,
                    Middle_name = form.textBoxMiddleName.Text,
                    Gradebook = int.Parse(form.maskedGradebook.Text),
                    Id_debts = debts.Id,
                    Debts = debts,
                    Id_Group = groups.Id,
                    Groups = groups
                };

                dbManager.TableStudents.UpdateByIdStudent(students);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте номер зачетки.Она не может быть как у других студентов");
                return;

            }
        }

    }
}

