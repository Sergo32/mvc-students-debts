﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model;
using WFA_University_Student_Debt.Model.Entities;
using System.Windows.Forms;

namespace WFA_University_Student_Debt.Controller
{
    class ControllerFormDebts
    {

        private FormDebts form;
        private DbManager dbManager;
        public ControllerFormDebts(FormDebts form)
        {
            this.form = form;
            dbManager = DbManager.GetInstance();
        }

        public void FillDataGridDebts()
        {
            form.dataGridViewDebts.DataSource = null;
            form.dataGridViewDebts.DataSource = dbManager.TableDebts.GetAllDebts();
      
        }


        public void ClearFields()
        {
          
            form.textBoxFio.Clear();
            form.textBoxDebtOnTheSubject.Clear();          
            form.textBoxId.Clear();

        }
        public void FillFromSelectedDebts()
        {
            if (form.dataGridViewDebts.SelectedRows.Count == 0)
            {
                return;
            }

            EntitieDebts debts = (EntitieDebts)form.dataGridViewDebts.SelectedRows[0].DataBoundItem;

            form.textBoxFio.Text = debts.Fio_teacher;
            form.textBoxDebtOnTheSubject.Text = debts.Debt_on_the_subject;
            form.dateTimePickerDebts.Text = debts.Date_of_results.ToString();
            form.textBoxId.Text = debts.Id.ToString();


        }

        public void AddDebts()
        {
            if (form.textBoxFio.Text == string.Empty)
            {
                MessageBox.Show("Заполните ФИО");
                return;
            }
            if (form.textBoxDebtOnTheSubject.Text == string.Empty)
            {
                MessageBox.Show("Заполните Предмет");
                return;
            }

            try
            {
           
            EntitieDebts debts = new EntitieDebts()
            {
                Id = int.Parse(form.textBoxId.Text),
                Debt_on_the_subject = form.textBoxDebtOnTheSubject.Text,
                Fio_teacher = form.textBoxFio.Text,
                Date_of_results = form.dateTimePickerDebts.Value
            };

            dbManager.TableDebts.AddDebts(debts);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте Имя предмета.Он не может быть Одинаковым");
                return;

            }

        }


        public void DeleteDebts()
        {
            if (form.textBoxId.Text == string.Empty)
            {
                MessageBox.Show("Задолжность выберите для удаления");
                return;
            }
            int id = int.Parse(form.textBoxId.Text);

            dbManager.TableDebts.DeleteByIdDebts(id);

        }

        public void UpdateDebts()
        {
            if (form.textBoxFio.Text == string.Empty)
            {
                MessageBox.Show("Заполните ФИО");
                return;
            }
            if (form.textBoxDebtOnTheSubject.Text == string.Empty)
            {
                MessageBox.Show("Заполните Предмет");
                return;
            }

            try
            {
          
            EntitieDebts debts = new EntitieDebts()
            {
                Id = int.Parse(form.textBoxId.Text),
                Debt_on_the_subject= form.textBoxDebtOnTheSubject.Text,
                Fio_teacher = form.textBoxFio.Text,
                Date_of_results=form.dateTimePickerDebts.Value
            };

            dbManager.TableDebts.UpdateByIdDebts(debts);
            }
            catch (Exception)
            {
                MessageBox.Show("Проверьте Имя предмета.Он не может быть Одинаковым");
                return;

            }

        }
    }
}
