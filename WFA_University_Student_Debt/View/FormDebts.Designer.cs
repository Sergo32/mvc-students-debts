﻿namespace WFA_University_Student_Debt
{
    partial class FormDebts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePickerDebts = new System.Windows.Forms.DateTimePicker();
            this.textBoxDebtOnTheSubject = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.textBoxFio = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewDebts = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxId = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDebts)).BeginInit();
            this.SuspendLayout();
            // 
            // dateTimePickerDebts
            // 
            this.dateTimePickerDebts.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dateTimePickerDebts.Location = new System.Drawing.Point(98, 26);
            this.dateTimePickerDebts.Name = "dateTimePickerDebts";
            this.dateTimePickerDebts.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerDebts.TabIndex = 0;
            // 
            // textBoxDebtOnTheSubject
            // 
            this.textBoxDebtOnTheSubject.Location = new System.Drawing.Point(446, 216);
            this.textBoxDebtOnTheSubject.Name = "textBoxDebtOnTheSubject";
            this.textBoxDebtOnTheSubject.Size = new System.Drawing.Size(128, 22);
            this.textBoxDebtOnTheSubject.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(443, 194);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Название Предмета";
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(446, 156);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(82, 23);
            this.buttonUpdate.TabIndex = 8;
            this.buttonUpdate.Text = "Обновить";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(446, 115);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(82, 23);
            this.buttonDelete.TabIndex = 7;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(446, 76);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(82, 23);
            this.buttonAdd.TabIndex = 6;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // textBoxFio
            // 
            this.textBoxFio.Location = new System.Drawing.Point(446, 263);
            this.textBoxFio.Name = "textBoxFio";
            this.textBoxFio.Size = new System.Drawing.Size(128, 22);
            this.textBoxFio.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(443, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(148, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "ФИО преподавателя";
            // 
            // dataGridViewDebts
            // 
            this.dataGridViewDebts.AllowUserToAddRows = false;
            this.dataGridViewDebts.AllowUserToDeleteRows = false;
            this.dataGridViewDebts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewDebts.Location = new System.Drawing.Point(2, 69);
            this.dataGridViewDebts.MultiSelect = false;
            this.dataGridViewDebts.Name = "dataGridViewDebts";
            this.dataGridViewDebts.ReadOnly = true;
            this.dataGridViewDebts.RowHeadersWidth = 51;
            this.dataGridViewDebts.RowTemplate.Height = 24;
            this.dataGridViewDebts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewDebts.Size = new System.Drawing.Size(435, 213);
            this.dataGridViewDebts.TabIndex = 13;
            this.dataGridViewDebts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewDebts_CellClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(562, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 17);
            this.label7.TabIndex = 23;
            this.label7.Text = "Id";
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(546, 157);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.ReadOnly = true;
            this.textBoxId.Size = new System.Drawing.Size(45, 22);
            this.textBoxId.TabIndex = 22;
            // 
            // FormDebts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 360);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxId);
            this.Controls.Add(this.dataGridViewDebts);
            this.Controls.Add(this.textBoxFio);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxDebtOnTheSubject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.dateTimePickerDebts);
            this.Name = "FormDebts";
            this.Text = "FormDebts";
            this.Load += new System.EventHandler(this.FormDebts_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDebts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.DateTimePicker dateTimePickerDebts;
        public System.Windows.Forms.TextBox textBoxDebtOnTheSubject;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button buttonUpdate;
        public System.Windows.Forms.Button buttonDelete;
        public System.Windows.Forms.Button buttonAdd;
        public System.Windows.Forms.TextBox textBoxFio;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.DataGridView dataGridViewDebts;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox textBoxId;
    }
}