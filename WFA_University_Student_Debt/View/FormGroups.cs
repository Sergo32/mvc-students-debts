﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFA_University_Student_Debt.Controller;

namespace WFA_University_Student_Debt
{
    public partial class FormGroups : Form
    {
        private ControllerFormGroups controller;
        public FormGroups()
        {
            InitializeComponent();
        }

        private void FormGroups_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormGroups(this);
            controller.FillListBoxGroups();
            controller.ClearFields();
        }

        private void listBoxGtopus_SelectedIndexChanged(object sender, EventArgs e)
        {
            controller.FillFieldsFromSelectedGroups();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            controller.AddGroup();
            controller.FillListBoxGroups();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            controller.DeleteGroup();
            controller.FillListBoxGroups();
            controller.ClearFields();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            controller.UpdateGroup();
            controller.FillListBoxGroups();
        }
    }
}
