﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFA_University_Student_Debt.Controller;

namespace WFA_University_Student_Debt
{
    public partial class FormMain : Form
    {

        private ControllerFormMain controller;
        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormMain(this);
            controller.FillComboBoxDebts();
            controller.FillComboBoxGroups();
            controller.FillDataGridStudents();
          
        }

        private void buttonUpdateGroups_Click(object sender, EventArgs e)
        {
            new FormGroups().ShowDialog();

            controller.FillComboBoxDebts();
            controller.FillComboBoxGroups();
            controller.FillDataGridStudents();
            controller.ClearFields();
        }

        private void buttonUpdateDebts_Click(object sender, EventArgs e)
        {
            new FormDebts().ShowDialog();
            controller.FillComboBoxDebts();
            controller.FillComboBoxGroups();
            controller.FillDataGridStudents();
            controller.ClearFields();
        }

     

        private void dataGridViewStudents_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controller.FillFromSelectedStudents();
        }

        private void buttonAddStudent_Click(object sender, EventArgs e)
        {
            controller.AddStudent();
            controller.FillComboBoxDebts();
            controller.FillComboBoxGroups();
            controller.FillDataGridStudents();
     

        }

        private void buttonDeleteStudent_Click(object sender, EventArgs e)
        {
            controller.DeleteStudent();
            controller.FillComboBoxDebts();
            controller.FillComboBoxGroups();
            controller.FillDataGridStudents();
            controller.ClearFields();

        }

        private void buttonUpdateStudent_Click(object sender, EventArgs e)
        {
            controller.UpdateStudent();
            controller.FillComboBoxDebts();
            controller.FillComboBoxGroups();
            controller.FillDataGridStudents();
           

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            controller.ClearFields();
        }
    }
}
