﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WFA_University_Student_Debt.Controller;

namespace WFA_University_Student_Debt
{
    public partial class FormDebts : Form
    {
        private ControllerFormDebts controller;
        public FormDebts()
        {
            InitializeComponent();
        }

        private void FormDebts_Load(object sender, EventArgs e)
        {
            controller = new ControllerFormDebts(this);
            controller.FillDataGridDebts();
            controller.ClearFields();
        }

        private void dataGridViewDebts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controller.FillFromSelectedDebts();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            controller.AddDebts();
            controller.FillDataGridDebts();
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            controller.DeleteDebts();
            controller.FillDataGridDebts();
            controller.ClearFields();
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            controller.UpdateDebts();
            controller.FillDataGridDebts();
        }
    }
}
