﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model.Table;

namespace WFA_University_Student_Debt.Model
{
    class DbManager
    {
        private static DbManager instance = null;

        public TableDebts TableDebts { get; set; }
        public TableGroups TableGroups { get; set; }
        public TableStudents TableStudents { get; set; }

        public DbManager()
        {
            TableDebts = new TableDebts();
            TableGroups = new TableGroups();
            TableStudents = new TableStudents();
        }

        public static DbManager GetInstance()
        {
            if (instance==null)
            {
                instance = new DbManager();
            }
            return instance;
        }

    }
}
