﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model.Entities;
using MySql.Data.MySqlClient;
using WFA_University_Student_Debt.Model.Tols;

namespace WFA_University_Student_Debt.Model.Table
{
    class TableDebts
    {
        public List<EntitieDebts> GetAllDebts()
        {

            List<EntitieDebts> Debts = new List<EntitieDebts>();
            MySqlCommand mySqlCommand = DbConnector.GetInstanse().GetSqlCommand();

            mySqlCommand.CommandText = "Call select_all_debts()";
            MySqlDataReader mySqlReader = mySqlCommand.ExecuteReader();

            while (mySqlReader.Read())
            {
                Debts.Add(new EntitieDebts()
                {
                    Id = mySqlReader.GetInt32("id"),
                    Debt_on_the_subject = mySqlReader.GetString("debt_on_the_subject"),
                    Fio_teacher = mySqlReader.GetString("fio_teacher"),
                    Date_of_results = mySqlReader.GetDateTime("date_of_results")
                });
            }

            mySqlReader.Close();
            return Debts;
        }


        public void AddDebts(EntitieDebts debts)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"Call add_debts('{debts.Debt_on_the_subject}','{debts.Fio_teacher}','{debts.Date_of_results:yyyy-MM-dd HH:mm:ss}')";
                sqlCommand.ExecuteNonQuery();
        }

        public void DeleteByIdDebts(int id)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"CALL delete_by_id_debts({id})";
            sqlCommand.ExecuteNonQuery();
        }

        public void UpdateByIdDebts(EntitieDebts debts)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"Call update_debts({debts.Id},'{debts.Debt_on_the_subject}','{debts.Fio_teacher}','{debts.Date_of_results:yyyy-MM-dd HH:mm:ss}')";
            sqlCommand.ExecuteNonQuery();
        }

    }


}
