﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model.Tols;
using WFA_University_Student_Debt.Model.Entities;
using MySql.Data.MySqlClient;

namespace WFA_University_Student_Debt.Model.Table
{
    class TableGroups
    {
        public List<EntitieGroups> GetAllGroups()
        {
            List<EntitieGroups> Groups = new List<EntitieGroups>();
            MySqlCommand mySqlCommand = DbConnector.GetInstanse().GetSqlCommand();

            mySqlCommand.CommandText = "Call selct_all_groups()";
            MySqlDataReader sqlDataReader = mySqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                Groups.Add(new EntitieGroups()
                {
                    Id = sqlDataReader.GetInt32("id"),
                    Name = sqlDataReader.GetString("name")

                });
            }
            sqlDataReader.Close();
            return Groups;
        }

        public void AddGroups(EntitieGroups groups)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"Call add_groups('{groups.Name}')";
            sqlCommand.ExecuteNonQuery();
        }

        public void DeleteByIdGroup(int id)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"CALL delete_by_id_group({id})";
            sqlCommand.ExecuteNonQuery();
        }

        public void UpdateByIdDebts(EntitieGroups groups)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"CALL update_groups({groups.Id},'{groups.Name}')";
            sqlCommand.ExecuteNonQuery();
        }
    }
}
