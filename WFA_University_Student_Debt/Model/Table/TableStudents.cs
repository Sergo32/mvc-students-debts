﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model.Entities;
using MySql.Data.MySqlClient;
using WFA_University_Student_Debt.Model.Tols;

namespace WFA_University_Student_Debt.Model.Table
{
    class TableStudents
    {
       
        public List<EntitieStudents> GetAllStudents(List<EntitieDebts> Debts, List<EntitieGroups>  Groups)
        {
            List<EntitieStudents> Students = new List<EntitieStudents>();
            MySqlCommand mySqlCommand = DbConnector.GetInstanse().GetSqlCommand();

            mySqlCommand.CommandText = "Call select_all_students()";
            MySqlDataReader mySqlReader = mySqlCommand.ExecuteReader();

            while (mySqlReader.Read())
            {
                EntitieStudents entitieStudents=new EntitieStudents()
                { 
                    Id = mySqlReader.GetInt32("id"),
                    Surname=mySqlReader.GetString("surname"),
                   Name = mySqlReader.GetString("name"),
                   Middle_name=mySqlReader.GetString("middle_name"),
                   Gradebook=mySqlReader.GetInt32("gradebook"),
                   Id_debts=mySqlReader.GetInt32("id_debts"),
                   Id_Group=mySqlReader.GetInt32("id_group")                
                };
                entitieStudents.Debts = Debts.Find(item => item.Id == entitieStudents.Id_debts);
                entitieStudents.Groups = Groups.Find(item => item.Id == entitieStudents.Id_Group);//ищем чтоб по id совпадает с idCategory
                Students.Add(entitieStudents);
            }

            mySqlReader.Close();
            return Students;
        }

        public void AddStudent(EntitieStudents students)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"Call add_student('{students.Surname}','{students.Name}','{students.Middle_name}',{students.Gradebook},{students.Id_debts},{students.Id_Group})";
            sqlCommand.ExecuteNonQuery();
        }

        public void DeleteByIdStudent(int id)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"CALL delete_by_id_student({id})";
            sqlCommand.ExecuteNonQuery();
        }

        public void UpdateByIdStudent(EntitieStudents students)
        {
            MySqlCommand sqlCommand = DbConnector.GetInstanse().GetSqlCommand();
            sqlCommand.CommandText = $"CALL update_student({students.Id},'{students.Surname}','{students.Name}','{students.Middle_name}',{students.Gradebook},{students.Id_debts},{students.Id_Group})";
            sqlCommand.ExecuteNonQuery();
        }
          //if (dbManager.TableCategories.ExsistName(category) == true)
          //  {
          //      MessageBox.Show("Такое имя существует");
          //      return false;

          //      public bool ExsistName(Category category)
          //      {
          //          return Rows.Exists(item => item.Name == category.Name);
          //      }
          //  }
    }
}
