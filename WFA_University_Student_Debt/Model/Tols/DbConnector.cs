﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WFA_University_Student_Debt.Model.Tols
{
    class DbConnector
    {

        private static  DbConnector instanse = null;
        private MySqlCommand sqlCommand;
        private MySqlConnection sqlConnection;

        private DbConnector()
        {
            string commands = "Server=127.0.0.1;UserId=root;Password=1234;Database=university";
            sqlConnection = new MySqlConnection(commands);

            sqlConnection.Open();
            sqlCommand = new MySqlCommand();
            sqlCommand.Connection = sqlConnection;

        }

        ~DbConnector()
        {
            sqlConnection.Close();
        }

        public MySqlCommand GetSqlCommand()
        {
            return sqlCommand;
        }

        public  static DbConnector GetInstanse()
        {
            if (instanse==null)
            {
                instanse = new DbConnector();
            }
            return instanse;
        }
    }
}
