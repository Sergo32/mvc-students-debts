﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFA_University_Student_Debt.Model.Entities;

namespace WFA_University_Student_Debt.Model.Entities
{
    class EntitieStudents
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Middle_name { get; set; }
        public int Gradebook { get; set; }
        public int Id_debts { get; set; }
        public int Id_Group { get; set; }

        public EntitieDebts Debts { get; set; }
        public EntitieGroups Groups { get; set; }

    }
}
