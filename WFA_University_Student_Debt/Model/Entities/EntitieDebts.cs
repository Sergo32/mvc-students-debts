﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WFA_University_Student_Debt.Model.Entities
{
    class EntitieDebts
    {
        public int Id { get; set; }
        public string Debt_on_the_subject { get; set; }
        public string Fio_teacher { get; set; }
        public DateTime Date_of_results { get; set; }

        public override string ToString()
        {
            return Debt_on_the_subject;
        }
    }
}
